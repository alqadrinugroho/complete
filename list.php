<?php  
require 'config.php';
$seminar = query("SELECT * FROM form");


// tombol cari ditekan
if( isset($_POST["cari"]) ) {
	$seminar = cari($_POST["search"]);
}


?>

<!DOCTYPE html>
<html>
<head>
	<title>Daftar Peserta</title>
	
</head>
<body>

	<center>

	<h1>Daftar Peserta</h1>

	<a href="daftar.php">Tambah Data Peserta</a>
	<br><br>

	<form action="" method="post">
		
		<input type="text" name="search" size="40" autofocus placeholder="Masukkan Keyword Pencarian..." autocomplete="off">
		<button type="submit" name="cari">Cari</button>

	</form>

	<br>

	<table border="0" class="table" width="100%" >
		
		<tr bgcolor="#CCCCCC">
			<th>No.</th>
			
			<th>Nama</th>
			<th>Email</th>
			<th>Nomor Telepon</th>
			<th>Tempat Lahir</th>
			<th>Tanggal Lahir</th>
			<th>Jenis Kelamin</th>
			<th>Alamat</th>
			<th>Jenis Instansi</th>
			<th>Nama Instansi</th>
			<th>Aksi</th>
		</tr>
		<?php $i = 1; ?>
		<?php foreach ($seminar as $row) : ?>
		<tr>
			<td><?= $i ?></td>
			
			<td><?= $row["nama"]; ?></td>
			<td><?= $row["email"]; ?></td>
			<td><?= $row["telp"]; ?></td>
			<td><?= $row["tempatlahir"]; ?></td>
			<td><?= $row["tanggallahir"]; ?></td>
			<td><?= $row["gender"]; ?></td>
			<td><?= $row["alamat"]; ?></td>
			<td><?= $row["jenisinstansi"]; ?></td>
			<td><?= $row["namainstansi"]; ?></td>
			<td>
				<a href="edit.php?id=<?= $row["id"]; ?>">Edit</a> |
				<a href="hapus.php?id=<?= $row["id"]; ?>" onclick="return confirm('Apakah Anda yakin ingin menghapus?');">Hapus</a>
			</td>
		</tr>
		<?php $i++; ?>
		<?php endforeach; ?>

	</table>

	</center>

</body>
</html>

