<?php 
require 'config.php';


// cek apakah tombol submit sudah ditekan atau belum
if ( isset($_POST["submit"]) ) {


	// cek apakah data berhasil di tambahkan atau tidak
	if( tambah($_POST) > 0 ) {
		echo "
			<script>
				alert('data berhasil ditambahkan!');
				document.location.href = 'list.php';
			</script>
		";
	} else {
		echo "
			<script>
				alert('data gagal ditambahkan!');
				document.location.href = 'list.php';
			</script>
		";
		echo mysqli_error($db);
	}
}


?>


<!DOCTYPE html>
<html>
<head>
	<title>Form Pendaftaran Seminar</title>
	
</head>
<body>
	<center>
	<h1>Form Pendaftaran Seminar</h1>
	<h2>"Routing The World"</h2>

	<form action="" method="post">
		<table >
			<input type="hidden" name="id" value="<?= $up["id"] ?>">
		<tr>
			<td colspan="2"><label for="nama">Nama</label></td>
		</tr>
		<tr>
			<td colspan="2"><input type="text" name="nama" id="nama" required></td>
		</tr>
		<tr>
			<td colspan="2"><label for="email">Email</label></td>
		</tr>
		<tr>
			<td colspan="2"><input type="email" name="email" id="email" required></td>
		</tr>
		<tr>
			<td colspan="2"><label for="telp">No Telp</label></td>
		</tr>
		<tr>
			<td colspan="2"><input type="phone" name="telp" id="telp" required></td>
		</tr>
		<tr>
			<td><label for="tempatlahir">Tempat Lahir</label></td>
			<td><label for="tanggallahir">Tanggal Lahir</label></td>
		</tr>
		<tr>
			<td><input type="text" name="tempatlahir" id="tempatlahir"></td>
			<td><input type="date" name="tanggallahir" id="tanggallahir"></td>
		</tr>
		<tr>
			<td><label for="gender">Jenis Kelamin</label></td>
		</tr>
		<tr>
			<td><input type="radio" name="gender" id="gender" value="Laki-laki"> Laki-laki</td>
		</tr>
		<tr>
			<td><input type="radio" name="gender" id="gender" value="Perempuan"> Perempuan</td>
		</tr>
		<tr>
			<td><input type="radio" name="gender" id="gender" value="Lainnya"> Lainnya</td>
		</tr>
		<tr>
			<td><label for="alamat">Alamat</label></td>
		</tr>
		<tr>
			<td><textarea type="text" name="alamat" id="alamat"></textarea></td>
		</tr>
		<tr>
			<td><label for="jenisinstansi">Jenis Instansi</label></td>
		</tr>
		<tr>
			<td>
				<select name="jenisinstansi">
                        <option></option>
                        <option>Badan Khusus</option>
                        <option>Negara</option>
                        <option>Swasta</option>
					<option>Lainnya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><label for="namainstansi">Nama Instansi :</label></td>
		</tr>
		<tr>
			<td><input type="text" name="namainstansi" id="namainstansi" required></td>
		</tr>
		<tr>
			<td></td>
		</tr>			
		</table>
		<button type="submit" name="submit">Daftar</button>
	</form>
	</center>
</body>
</html>