<?php 
require 'config.php';


//ambil data di URL
$id = $_GET["id"];


// query data seminar berdasarkan id
$up = query("SELECT * FROM form WHERE id = $id")[0];


// cek apakah tombol submit sudah ditekan atau belum
if ( isset($_POST["submit"]) ) {


	// cek apakah data berhasil diedit atau tidak
	if( ubah($_POST) > 0 ) {
		echo "
			<script>
				alert('data berhasil diedit!');
				document.location.href = 'list.php';
			</script>
		";
	} else {
		echo "
			<script>
				alert('data gagal diedit!');
				document.location.href = 'list.php';
			</script>
		";
	}
}

?>


<!DOCTYPE html>
<html>
<head>
	<title>Form Pendaftaran Seminar</title>
</head>
<body>
	<center>
	<h1>Form Pendaftaran Seminar</h1>


	<form action="" method="post">
		<table >
			<input type="hidden" name="id" value="<?= $up["id"] ?>">
		<tr>
			<td colspan="2"><label for="nama">Nama :</label></td>
		</tr>
		<tr>
			<td colspan="2"><input type="text" name="nama" id="nama" required value="<?= $up["nama"] ?>"></td>
		</tr>
		<tr>
			<td colspan="2"><label for="email">Email :</label></td>
		</tr>
		<tr>
			<td colspan="2"><input type="email" name="email" id="email" required value="<?= $up["email"] ?>"></td>
		</tr>
		<tr>
			<td colspan="2"><label for="telp">No Telp :</label></td>
		</tr>
		<tr>
			<td colspan="2"><input type="phone" name="telp" id="telp" value="<?= $up["telp"] ?>"></td>
		</tr>
		<tr>
			<td><label for="tempatlahir">Tempat Lahir :</label></td>
			<td><label for="tanggallahir">Tanggal Lahir :</label></td>
		</tr>
		<tr>
			<td><input type="text" name="tempatlahir" id="tempatlahir" value="<?= $up["tempatlahir"] ?>"></td>
			<td><input type="date" name="tanggallahir" id="tanggallahir" value="<?= $up["tanggallahir"] ?>"></td>
		</tr>
		<tr>
			<td><label for="gender">Jenis Kelamin</label></td>
			<?php $gender = $up['gender']; ?>
		</tr>
		<tr>
			<td><input type="radio" name="gender" id="gender" value="Laki-laki"<?php echo ($gender == 'Laki-laki') ? "checked": "" ?>> Laki-laki</td>
		</tr>
		<tr>
			<td><input type="radio" name="gender" id="gender" value="Perempuan"<?php echo ($gender == 'Perempuan') ? "checked": "" ?>> Perempuan</td>
		</tr>
		<tr>
			<td><input type="radio" name="gender" id="gender" value="Lainnya"<?php echo ($gender == 'Lainnya') ? "checked": "" ?>> Lainnya</td>
		</tr>
		<tr>
			<td><label for="alamat">Alamat</label></td>
		</tr>
		<tr>
			<td><textarea type="text" name="alamat" id="alamat"><?php echo $up['alamat'] ?></textarea></td>
		</tr>
		<tr>
			<td><label for="jenisinstansi">Jenis Instansi</label></td>
		</tr>
		<tr>
			<td>
				<select name="jenisinstansi">
                        <?php $jenisi = $up['jenisinstansi']; ?>
                        <option></option>
                        <option <?php echo ($jenisi == 'Badan Khusus') ? "selected": "" ?>>Badan Khusus</option>
                        <option <?php echo ($jenisi == 'Negara') ? "selected": "" ?>>Negara</option>
                        <option <?php echo ($jenisi == 'Swasta') ? "selected": "" ?>>Swasta</option>
					<option <?php echo ($jenisi == 'Lainnya') ? "selected": "" ?>>Lainnya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><label for="namainstansi">Nama Instansi :</label></td>
		</tr>
		<tr>
			<td><input type="text" name="namainstansi" id="namainstansi" required value="<?= $up["namainstansi"] ?>"></td>
		</tr>
		<tr>
			<td></td>
		</tr>			
		</table>
		<button type="submit" name="submit">Edit</button>
	</form>
	</center>
</body>
</html>