<?php

$server = "localhost";
$user = "root";
$password = "";
$nama_database = "tugasform";

$db = mysqli_connect($server, $user, $password, $nama_database);

if( !$db ){
    die("Gagal terhubung dengan database: " . mysqli_connect_error());
}

function query($query){
	global $db;
	$sql = mysqli_query($db, $query);
	$rows = [];
	while ( $row = mysqli_fetch_assoc($sql) ) {
		$rows[] = $row;
	}
	return $rows;
}

function tambah($data) {
	global $db;

	//ambil data dari tiap elemen dalam form
	$nama = htmlspecialchars($data["nama"]);
	$email = htmlspecialchars($data["email"]);
	$telp = htmlspecialchars($data["telp"]);
	$place = htmlspecialchars($data["tempatlahir"]);
	$date = htmlspecialchars($data["tanggallahir"]);
	$gender = ($data["gender"]);
	$alamat = htmlspecialchars($data["alamat"]);
	$jenisi = $data["jenisinstansi"];
	$namai = htmlspecialchars($data["namainstansi"]);


	// query insert data
	$query = "INSERT INTO form VALUES
				('', '$nama', '$email', '$telp', '$place', '$date', '$gender', '$alamat', '$jenisi', '$namai')
				";
	mysqli_query($db, $query);

	return mysqli_affected_rows($db);
}



function hapus($id) {
	global $db;

	mysqli_query($db, "DELETE FROM form WHERE id = $id");

	return mysqli_affected_rows($db);
}



function ubah($data) {
	global $db;

	//ambil data dari tiap elemen dalam form
	$id = $data["id"];
	$nama = htmlspecialchars($data["nama"]);
	$email = htmlspecialchars($data["email"]);
	$telp = htmlspecialchars($data["telp"]);
	$place = htmlspecialchars($data["tempatlahir"]);
	$date = htmlspecialchars($data["tanggallahir"]);
	$gender = ($data["gender"]);
	$alamat = htmlspecialchars($data["alamat"]);
	$jenisi = $data["jenisinstansi"];
	$namai = htmlspecialchars($data["namainstansi"]);


	// query insert data
	$query = "UPDATE form SET
				nama = '$nama',
				email = '$email',
				telp = '$telp',
				tempatlahir = '$place',
				tanggallahir = '$date',
				gender = '$gender',
				alamat = '$alamat',
				jenisinstansi = '$jenisi',
				namainstansi = '$namai'
			  WHERE id = $id";
	mysqli_query($db, $query);

	return mysqli_affected_rows($db);
}


function cari($search) {
	$query = "SELECT * FROM form
				WHERE
			  nama LIKE '%$search%' OR 
			  email LIKE '%$search%' OR
			  telp LIKE '%$search%' OR 
			  tempatlahir LIKE '%$search%' OR
			  tanggallahir LIKE '%$search%' OR 
			  gender LIKE '%$search%' OR
			  alamat LIKE '%$search%' OR 
			  jenisinstansi LIKE '%$search%' OR 
			  namainstansi LIKE '%$search%'";

	return query($query);
}


// function registrasi($data) {
// 	global $conn;

// 	$username = htmlspecialchars(strtolower(stripslashes($data["username"])));
// 	$password = htmlspecialchars(mysqli_real_escape_string($conn, $data["password"]));
// 	$password2 = htmlspecialchars(mysqli_real_escape_string($conn, $data["password2"]));
// 	$email = htmlspecialchars($data["email"]);
// 	$asal = htmlspecialchars($data["asal"]);

// 	// cek username sudah ada atau belum
// 	$result = mysqli_query($conn, "SELECT username FROM users WHERE username = '$username' ");
// 	if (mysqli_fetch_assoc($result)) {
// 		echo "<script>
// 				alert('username sudah terdaftar !');
// 				</script>";
// 		return false;
// 	}

// 	//cek konfirmasi password
// 	if ($password !== $password2) {
// 		echo "<script>
// 				alert('konfirmasi password tidak sesuai !');
// 				</script>";	
// 		return false;
// 	}

// 	//enkripsi password
// 	$password = password_hash($password, PASSWORD_DEFAULT);

// 	//tambahkan userbaru ke database
// 	mysqli_query($conn, "INSERT INTO users VALUES('', '$username', '$password', '$email', '$asal' )");

// 	return mysqli_affected_rows($conn);

// }



?>